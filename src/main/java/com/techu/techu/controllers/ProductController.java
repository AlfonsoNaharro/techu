package com.techu.techu.controllers;

import com.techu.techu.TechuApplication;
import com.techu.techu.models.ProductModel;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class ProductController {

    static final String APIBaseUrl = "/apitechu/v1";

    @GetMapping(APIBaseUrl + "/products")
    public ArrayList<ProductModel> getProducts (){
        System.out.println("getProducts");

        return TechuApplication.productModels;
    }
    @GetMapping(APIBaseUrl + "/products/{id}")
    public ProductModel getProductById(@PathVariable String id){
        System.out.println("getProductById");
        System.out.println("La id del producto a obtener es " + id);

        ProductModel result = new ProductModel();

        for (ProductModel product : TechuApplication.productModels) {
            if (product.getId().equals(id)){
                System.out.println("Producto encontrado");
                result = product;
            }
        }
        return result;
    }

    @PostMapping(APIBaseUrl + "/products")
    public ProductModel createProduct(@RequestBody ProductModel newProduct){
        System.out.println("createProduct");
        System.out.println("La id del producto a crear es " + newProduct.getId());
        System.out.println("la descripción del producto a crear es " + newProduct.getDesc());
        System.out.println("El precio del producto a crear es " + newProduct.getPrice());

        TechuApplication.productModels.add(newProduct);

        return new ProductModel();
    }
    @PutMapping(APIBaseUrl + "/products/{id}")
    public ProductModel updateProduct(@RequestBody ProductModel product, @PathVariable String id) {
        System.out.println("updateProduct");
        System.out.println("La id del producto a actualizar es " + id);
        System.out.println("La id del producto a actualizar es " + product.getId());
        System.out.println("la descripción del producto a actualizar es " + product.getDesc());
        System.out.println("El precio del producto a actualizar es " + product.getPrice());

        for (ProductModel productInList : TechuApplication.productModels){
            if (productInList.getId().equals(id)){
                productInList.setId(product.getId());
                productInList.setDesc(product.getDesc());
                productInList.setPrice(product.getPrice());
            }
        }
        return product;
    }
    @PatchMapping(APIBaseUrl + "/products/{id}")
    public ProductModel patchProduct(@RequestBody ProductModel productData, @PathVariable String id){
        System.out.println("patchProduct");
        System.out.println("La id del producto a actualizar es " + id);
        System.out.println("la descripción del producto a actualizar es " + productData.getDesc());
        System.out.println("El precio del producto a actualizar es " + productData.getPrice());

        ProductModel result = new ProductModel();

        for (ProductModel productInList : TechuApplication.productModels){
            if (productInList.getId().equals(id)){
                System.out.println("Prodducto encontrado");
                result = productInList;

                if (productData.getDesc() !=null){
                    System.out.println("Actualizando descripción del producto");
                    productInList.setDesc(productData.getDesc());
                }

                if (productData.getPrice() >0 ){
                    System.out.println("Actualizando precio del producto");
                    productInList.setPrice(productData.getPrice());
                }
            }
        }

        return result;
    }

    @DeleteMapping(APIBaseUrl + "/products/{id}")
    public ProductModel deleteProduct(@PathVariable String id){
        System.out.println("deleteProduct");
        System.out.println("La id del producto a borrar es " + id);

        ProductModel result = new ProductModel();
        boolean foundCompany = false;

        for (ProductModel productInList : TechuApplication.productModels){
            if (productInList.getId().equals(id)){
                System.out.println("Producto Encontrado");
                foundCompany = true;
                result = productInList;
            }
        }
        if (foundCompany == true){
            System.out.println("Borrando producto");
            TechuApplication.productModels.remove(result);
        }

        return result; //SOAPUI programa similar a postman
    }

}
